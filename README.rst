SciScripts  
=========

**SciScripts** is a Python library for controlling devices/running experiments/analyzing data.

For installation, configuration and more, see the `package documentation <https://sciscripts.readthedocs.io/en/stable>`_
