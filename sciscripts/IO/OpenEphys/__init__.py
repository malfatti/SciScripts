#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

:Author: T. Malfatti <malfatti@disroot.org>

:Date: 20170612

:License: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>

:Homepage: https://gitlab.com/Malfatti/SciScripts
"""

from sciscripts.IO.OpenEphys import OpenEphys

from sciscripts.IO.OpenEphys import Binary
from sciscripts.IO.OpenEphys import OpenEphysFormat
from sciscripts.IO.OpenEphys import SettingsXML
