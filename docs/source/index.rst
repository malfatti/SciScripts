Welcome to SciScripts's documentation!
======================================

**SciScripts** is a Python library for controlling devices/running experiments/analyzing data.

The package consists in three modules: `Analysis`, `Exps` and `IO`. In combination with a hardware setup, it allows stimulus delivery, recording control and analysis of several behavioral and electrophysiological techniques, including:

- Auditory Brainstem Responses (ABRs);
- Auditory Pre-pulse inhibition (PPI);
- Gap Pre-pulse Inhibition of Acoustic Startle (GPIAS);
- Auditory Evoked Response Potentials (aERPs);
- Extracellular Unit recordings;
- Local field potentials;
- Calcium imaging with miniscope;
- General optogenetic and/or sound stimulation;
- General signal processing and analysis;
- Statistical analysis.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Installation
   Dependencies
   Configuration
   Modules
   Examples



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
